/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include "callbacks.h"

GPtrArray *
get_cpu_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_cpu cpu;
	gint i;

	GString *xcpu_total_str, *xcpu_user_str, *xcpu_nice_str;
	GString *xcpu_sys_str, *xcpu_idle_str;

	glibtop_get_cpu (&cpu);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.total));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.user));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.nice));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.sys));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.idle));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", cpu.frequency));

	xcpu_total_str = g_string_new (NULL);
	xcpu_user_str = g_string_new (NULL);
	xcpu_nice_str = g_string_new (NULL);
	xcpu_sys_str = g_string_new (NULL);
	xcpu_idle_str = g_string_new (NULL);

	for (i = 0; i < glibtop_global_server->ncpu; i++) {
		gchar *xcpu_total = g_strdup_printf
			("%12Ld", cpu.xcpu_total [i]);
		gchar *xcpu_user = g_strdup_printf
			("%12Ld", cpu.xcpu_user [i]);
		gchar *xcpu_nice = g_strdup_printf
			("%12Ld", cpu.xcpu_nice [i]);
		gchar *xcpu_sys = g_strdup_printf
			("%12Ld", cpu.xcpu_sys [i]);
		gchar *xcpu_idle = g_strdup_printf
			("%12Ld", cpu.xcpu_idle [i]);

		g_string_append (xcpu_total_str, xcpu_total);
		g_string_append (xcpu_user_str, xcpu_user);
		g_string_append (xcpu_nice_str, xcpu_nice);
		g_string_append (xcpu_sys_str, xcpu_sys);
		g_string_append (xcpu_idle_str, xcpu_idle);

		g_free (xcpu_total); g_free (xcpu_user); g_free (xcpu_nice);
		g_free (xcpu_sys); g_free (xcpu_idle);
	}

	g_ptr_array_add (retval, g_strdup (xcpu_total_str->str));
	g_ptr_array_add (retval, g_strdup (xcpu_user_str->str));
	g_ptr_array_add (retval, g_strdup (xcpu_nice_str->str));
	g_ptr_array_add (retval, g_strdup (xcpu_sys_str->str));
	g_ptr_array_add (retval, g_strdup (xcpu_idle_str->str));

	g_string_free (xcpu_total_str, TRUE);
	g_string_free (xcpu_user_str, TRUE);
	g_string_free (xcpu_nice_str, TRUE);
	g_string_free (xcpu_sys_str, TRUE);
	g_string_free (xcpu_idle_str, TRUE);

	return retval;
}

GPtrArray *
get_mem_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_mem mem;

	glibtop_get_mem (&mem);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.total));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.used));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.free));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.shared));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.buffer));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.cached));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.user));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", mem.locked));

	return retval;
}

GPtrArray *
get_swap_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_swap swap;

	glibtop_get_swap (&swap);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", swap.total));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", swap.used));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", swap.free));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", swap.pagein));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", swap.pageout));

	return retval;
}

GPtrArray *
get_loadavg_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_loadavg data;

	glibtop_get_loadavg (&data);

	g_ptr_array_add (retval, g_strdup_printf
			 ("%8.3f %8.3f %8.3f", data.loadavg [0],
			  data.loadavg [1], data.loadavg [2]));
	g_ptr_array_add (retval, g_strdup_printf ("%8Ld", data.nr_running));
	g_ptr_array_add (retval, g_strdup_printf ("%8Ld", data.nr_tasks));
	g_ptr_array_add (retval, g_strdup_printf ("%8Ld", data.last_pid));

	return retval;
}

GPtrArray *
get_uptime_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_uptime data;

	glibtop_get_uptime (&data);

	g_ptr_array_add (retval, g_strdup_printf ("%12.3f", data.uptime));
	g_ptr_array_add (retval, g_strdup_printf ("%12.3f", data.idletime));

	return retval;
}

GPtrArray *
get_shm_limits_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_shm_limits data;

	glibtop_get_shm_limits (&data);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.shmmax));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.shmmin));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.shmmni));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.shmseg));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.shmall));

	return retval;
}

GPtrArray *
get_msg_limits_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_msg_limits data;

	glibtop_get_msg_limits (&data);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgpool));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgmap));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgmax));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgmnb));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgmni));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgssz));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.msgtql));

	return retval;
}

GPtrArray *
get_sem_limits_data (GtkWidget *clist)
{
	GPtrArray *retval = g_ptr_array_new ();
	glibtop_sem_limits data;

	glibtop_get_sem_limits (&data);

	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semmap));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semmni));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semmns));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semmnu));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semmsl));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semopm));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semume));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semusz));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semvmx));
	g_ptr_array_add (retval, g_strdup_printf ("%12Ld", data.semaem));

	return retval;
}
