/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <config.h>
#include <gnome.h>

#include <glibtop.h>
#include <glibtop/union.h>

#include "callbacks.h"

static void prepare_app (void);
static GtkWidget *create_notebook_page (gint, const char **, const unsigned *,
					const char **, const char **);
static void update_data (GtkWidget *, GPtrArray * (*) (GtkWidget *));
static void set_page_geometry (GtkWidget *);

static guint max_name_width = 0, max_label_width = 0;

static gint auto_update = FALSE;
static guint auto_update_interval = 1000;
static GPtrArray *clist_array = NULL;
GtkWidget *app = NULL, *notebook = NULL;

static void
quit_cb (GtkWidget *widget, void *data)
{
	guint x, y, width, height;

	gdk_window_get_geometry
		(notebook->window, &x, &y, &width, &height, NULL);

	gnome_config_push_prefix ("/libgtop-monitor/");

	gnome_config_set_int ("geometry/width", width);
	gnome_config_set_int ("geometry/height", height);

	gnome_config_pop_prefix ();
	gnome_config_sync ();

	gtk_main_quit ();
	return;
}

static void
update_cb (void)
{
	update_data (clist_array->pdata [0], get_cpu_data);
	update_data (clist_array->pdata [1], get_mem_data);
	update_data (clist_array->pdata [2], get_swap_data);
	update_data (clist_array->pdata [3], get_loadavg_data);
	update_data (clist_array->pdata [4], get_uptime_data);
	update_data (clist_array->pdata [5], get_shm_limits_data);
	update_data (clist_array->pdata [6], get_msg_limits_data);
	update_data (clist_array->pdata [7], get_sem_limits_data);
}

static void
timeout_cb (void)
{
	if (auto_update) update_cb ();
}

static void
auto_update_cb (GtkWidget *widget)
{
	auto_update = GTK_TOGGLE_BUTTON (widget)->active;

	gnome_config_set_int ("update/auto", auto_update);
	gnome_config_sync ();
}

static void
about_cb (GtkWidget *widget, void *data)
{
	GtkWidget *about;
	const gchar *authors[] = {
		"Martin Baulig",
		NULL,
	};
	
	about = gnome_about_new (_("LibGTop Monitor"), VERSION,
				 /* copyright notice */
				 _("(C) 1998 the Free Software Foundation"),
				 authors,
				 /* another comments */
				 _("This tool dumps the raw data returned "
				   "from LibGTop."),
				 NULL);
	gtk_widget_show (about);
	
	return;
}

static GnomeUIInfo help_menu [] = {
	GNOMEUIINFO_ITEM_STOCK (N_("About ..."), NULL, about_cb,
				GNOME_STOCK_MENU_ABOUT),
	GNOMEUIINFO_END
};

static GnomeUIInfo file_menu [] = {
	GNOMEUIINFO_ITEM_STOCK (N_("Exit"), NULL, quit_cb,
				GNOME_STOCK_MENU_EXIT),
	GNOMEUIINFO_END
};
	
static GnomeUIInfo main_menu [] = {
	GNOMEUIINFO_SUBTREE (N_("File"), &file_menu),
	GNOMEUIINFO_SUBTREE (N_("Help"), &help_menu),
	GNOMEUIINFO_END
};

int
main (int argc, char *argv[])
{
	/* Initialize the i18n stuff */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

       	gnome_init ("libgtop-monitor", NULL, argc, argv);

	/*
	 * prepare_app() makes all the gtk calls necessary to set up a
	 * minimal Gnome application; It's based on the hello world example
	 * from the Gtk+ tutorial
	 */
	prepare_app ();
	
	gtk_main ();
	
	return 0;
}

static void
prepare_app (void)
{
	GtkWidget *vbox, *vb, *bbox, *hbox;
	GtkWidget *separator, *label, *pixmap, *button;
	gint restore, i;

	/*
	 * Make the main window and binds the delete event so you can close
	 * the program from your WM
	 */
	app = gnome_app_new ("libgtop-monitor", _("LibGTop Monitor"));
	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			    GTK_SIGNAL_FUNC (quit_cb),
			    NULL);
	
	/* Now that we've the main window we'll make the menues */
	gnome_app_create_menus (GNOME_APP (app), main_menu);

	gnome_config_push_prefix ("/libgtop-monitor/");

	clist_array = g_ptr_array_new ();

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_CPU, glibtop_names_cpu,
			  glibtop_types_cpu, glibtop_labels_cpu,
			  glibtop_descriptions_cpu));
			    
	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_MEM, glibtop_names_mem,
			  glibtop_types_mem, glibtop_labels_mem,
			  glibtop_descriptions_mem));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_SWAP, glibtop_names_swap,
			  glibtop_types_swap, glibtop_labels_swap,
			  glibtop_descriptions_swap));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_LOADAVG, glibtop_names_loadavg,
			  glibtop_types_loadavg, glibtop_labels_loadavg,
			  glibtop_descriptions_loadavg));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_UPTIME, glibtop_names_uptime,
			  glibtop_types_uptime, glibtop_labels_uptime,
			  glibtop_descriptions_uptime));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_SHM_LIMITS, glibtop_names_shm_limits,
			  glibtop_types_shm_limits, glibtop_labels_shm_limits,
			  glibtop_descriptions_shm_limits));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_MSG_LIMITS, glibtop_names_msg_limits,
			  glibtop_types_msg_limits, glibtop_labels_msg_limits,
			  glibtop_descriptions_msg_limits));

	g_ptr_array_add (clist_array, create_notebook_page
			 (GLIBTOP_MAX_SEM_LIMITS, glibtop_names_sem_limits,
			  glibtop_types_sem_limits, glibtop_labels_sem_limits,
			  glibtop_descriptions_sem_limits));

	for (i = 0; i < clist_array->len; i++)
		set_page_geometry (clist_array->pdata [i]);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);

	notebook = gtk_notebook_new ();

	update_cb ();

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [0],
				  gtk_label_new (_("CPU")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [1],
				  gtk_label_new (_("Memory")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [2],
				  gtk_label_new (_("Swap")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [3],
				  gtk_label_new (_("Loadavg")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [4],
				  gtk_label_new (_("Uptime")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [5],
				  gtk_label_new (_("ShmLimits")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [6],
				  gtk_label_new (_("MsgLimits")));

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), clist_array->pdata [7],
				  gtk_label_new (_("SemLimits")));

	restore = gnome_config_get_int ("geometry/restore=0");

	if (restore) {
		guint width, height;

		width = gnome_config_get_int ("geometry/width=0");
		height = gnome_config_get_int ("geometry/height=0");

		if (width && height)
			gtk_widget_set_usize (notebook, width, height);
	}

	gtk_box_pack_start (GTK_BOX (vbox), notebook,
			    TRUE, TRUE, GNOME_PAD_SMALL);

	vb = gtk_vbox_new (TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (vb), GNOME_PAD_SMALL);
	
	auto_update = gnome_config_get_int ("update/auto=0");
	auto_update_interval = gnome_config_get_int ("update/interval=1000");

	button = gtk_check_button_new_with_label
		(_("Automatically update information"));
	gtk_signal_connect (GTK_OBJECT (button), "toggled",
			    auto_update_cb, NULL);
	gtk_toggle_button_set_state
		(GTK_TOGGLE_BUTTON (button), auto_update);
	gtk_container_add (GTK_CONTAINER (vb), button);

	gtk_box_pack_start (GTK_BOX (vbox), vb, FALSE, FALSE, 0);

	separator = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (vbox), separator, FALSE, TRUE, 0);

	bbox = gtk_hbutton_box_new ();
	gtk_container_border_width (GTK_CONTAINER (bbox), GNOME_PAD);

	button = gtk_button_new ();
	pixmap = gnome_stock_pixmap_widget (button, GNOME_STOCK_PIXMAP_REFRESH);
	label = gtk_label_new (_("Update Information ..."));
	hbox  = gtk_hbox_new (FALSE, GNOME_PAD_SMALL/2);
	gtk_box_pack_start (GTK_BOX (hbox), pixmap, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_signal_connect (GTK_OBJECT (button), "pressed", update_cb, NULL);
	gtk_container_add (GTK_CONTAINER (bbox), button);

#if 0
	button = gtk_button_new ();
	pixmap = gnome_stock_pixmap_widget (button, GNOME_STOCK_PIXMAP_BOOK_OPEN);
	label = gtk_label_new (_("Information about Processes ..."));
	hbox  = gtk_hbox_new (FALSE, GNOME_PAD_SMALL/2);
	gtk_box_pack_start (GTK_BOX (hbox), pixmap, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_container_add (GTK_CONTAINER (bbox), button);
#endif

	button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_signal_connect (GTK_OBJECT (button), "pressed", quit_cb, NULL);
	gtk_container_add (GTK_CONTAINER (bbox), button);

	gtk_box_pack_start (GTK_BOX (vbox), bbox, FALSE, FALSE, GNOME_PAD_SMALL);

	gtk_timeout_add (auto_update_interval,
			 (GtkFunction) timeout_cb,
			 GTK_OBJECT (app));

	gnome_app_set_contents (GNOME_APP (app), vbox);

	gtk_widget_show_all (app);
}

static void
set_page_geometry (GtkWidget *clist)
{
	gtk_clist_set_column_min_width (GTK_CLIST (clist), 0, max_name_width);
	gtk_clist_set_column_min_width (GTK_CLIST (clist), 1, max_label_width);
}

static void
update_data (GtkWidget *clist, GPtrArray * (*update_func) (GtkWidget *))
{
	GPtrArray *data = update_func (clist);
	gint i;

	for (i = 0; i < data->len; i++) {
		gtk_clist_set_text (GTK_CLIST (clist), i, 2, data->pdata [i]);
		g_free (data->pdata [i]);
	}

	g_ptr_array_free (data, TRUE);
}

static GtkWidget *
create_notebook_page (gint number,
		      const char *libgtop_names [],
		      const unsigned libgtop_types [],
		      const char *libgtop_labels [],
		      const char *libgtop_descriptions [])
{
	GtkWidget *clist;
	gchar *clist_titles [] = { N_("Name"), N_("Label"), N_("Value") };
	gint i;

	clist = gtk_clist_new_with_titles (3, clist_titles);

	gtk_widget_ensure_style (clist);

	gtk_clist_freeze (GTK_CLIST (clist));

	for (i = 0; i < number; i++) {
		gchar *data [3];
		GtkStyle *style;

		guint name_width, label_width;

		data [0] = g_strdup (libgtop_names [i]);
		data [1] = g_strdup (libgtop_labels [i]);
		data [2] = g_strdup ("");

		gtk_clist_append (GTK_CLIST (clist), data);

		name_width = gdk_string_width
			(clist->style->font, libgtop_names [i]);

		label_width = gdk_string_width
			(clist->style->font, libgtop_labels [i]);

		if (name_width > max_name_width)
			max_name_width = name_width;

		if (label_width > max_label_width)
			max_label_width = label_width;

		g_free (data [0]);
		g_free (data [1]);
		g_free (data [2]);
	}

	gtk_clist_thaw (GTK_CLIST (clist));

	return clist;
}
