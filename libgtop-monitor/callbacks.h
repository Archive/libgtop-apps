/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__ 1

#include <config.h>
#include <gnome.h>

#include <glibtop.h>
#include <glibtop/union.h>

BEGIN_GNOME_DECLS

extern GPtrArray *	get_cpu_data		(GtkWidget *);
extern GPtrArray *	get_mem_data		(GtkWidget *);
extern GPtrArray *	get_swap_data		(GtkWidget *);
extern GPtrArray *	get_loadavg_data	(GtkWidget *);
extern GPtrArray *	get_uptime_data		(GtkWidget *);
extern GPtrArray *	get_shm_limits_data	(GtkWidget *);
extern GPtrArray *	get_msg_limits_data	(GtkWidget *);
extern GPtrArray *	get_sem_limits_data	(GtkWidget *);

END_GNOME_DECLS

#endif /* __CALLBACKS_H__ */
