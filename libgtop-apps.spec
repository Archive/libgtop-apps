# Note that this is NOT a relocatable package
%define ver      0.25.0
%define rel      SNAP
%define prefix   /usr

Summary: LibGTop applications
Name: libgtop-apps
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Libraries
Source: ftp://ftp.home-of-linux.org/pub/libgtop-apps-%{ver}.tar.gz
BuildRoot: /tmp/libgtop-apps-root
Packager: Martin Baulig <martin@home-of-linux.org>
URL: http://www.home-of-linux.org/gnome/libgtop/
Prereq: /sbin/install-info
Docdir: %{prefix}/doc

%description

GNOME Applets and applications that have been rewritten to use LibGTop.

%changelog

* Fri Aug 21 1998 Martin Baulig <martin@home-of-linux.org>

- released LibGTop-Apps 0.25.0

%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
#rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%{prefix}/bin/*
%{prefix}/share/*

